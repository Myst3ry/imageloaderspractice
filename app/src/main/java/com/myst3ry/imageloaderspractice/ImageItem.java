package com.myst3ry.imageloaderspractice;

public interface ImageItem {

    ImageType getImageType();
}
