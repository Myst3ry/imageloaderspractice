package com.myst3ry.imageloaderspractice;

import com.myst3ry.imageloaderspractice.data.model.Image;
import com.myst3ry.imageloaderspractice.data.remote.pojo.ImageHit;

import java.util.ArrayList;
import java.util.List;

public final class ImageTypesHelper {

    private static final int TYPE_HTTP = 0;
    private static final int TYPE_PICASSO = 1;
    private static final int TYPE_GLIDE = 2;
    private static final int TYPE_FRESCO = 3;

    public static List<ImageItem> mapToTypedImages(final List<ImageHit> imageHits) {
        final List<ImageItem> images = new ArrayList<>();
        for (int i = 0; i < imageHits.size(); i++) {
            images.add(mapToTypedImage(i, imageHits.get(i)));
        }
        return images;
    }

    private static ImageItem mapToTypedImage(final int position, final ImageHit hit) {
        final int type = position % ImageType.values().length;
        switch (type) {
            case TYPE_HTTP:
                return new Image(ImageType.HTTP, hit.getWebformatURL());
            case TYPE_PICASSO:
                return new Image(ImageType.PICASSO, hit.getWebformatURL());
            case TYPE_GLIDE:
                return new Image(ImageType.GLIDE, hit.getWebformatURL());
            case TYPE_FRESCO:
                return new Image(ImageType.FRESCO, hit.getWebformatURL());
            default:
                return null;
        }
    }

    //constr
}
