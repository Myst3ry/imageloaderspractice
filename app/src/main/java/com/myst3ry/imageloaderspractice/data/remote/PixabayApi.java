package com.myst3ry.imageloaderspractice.data.remote;

import com.myst3ry.imageloaderspractice.data.remote.pojo.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PixabayApi {

    @GET("api/")
    Call<ApiResponse> getImagesByQuery(@Query("key") final String key,
                                       @Query("q") final String query,
                                       @Query("image_type") final String imageType,
                                       @Query("per_page") final int imagesPerPage);
}
