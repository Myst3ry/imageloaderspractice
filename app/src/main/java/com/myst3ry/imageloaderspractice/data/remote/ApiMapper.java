package com.myst3ry.imageloaderspractice.data.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.myst3ry.imageloaderspractice.OnImagesReceivedCallback;
import com.myst3ry.imageloaderspractice.data.remote.pojo.ApiResponse;
import com.myst3ry.imageloaderspractice.data.remote.pojo.ImageHit;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class ApiMapper {

    private static final String TAG = "ApiMapper";
    private static final String API_KEY = "9599599-d0aa7baf9ab3a4c34b8ad721c";

    private RetrofitHelper mHelper;
    private static ApiMapper INSTANCE;

    public static ApiMapper getInstance(final RetrofitHelper helper) {
        ApiMapper instance = INSTANCE;
        if (instance == null) {
            synchronized (ApiMapper.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new ApiMapper(helper);
                }
            }
        }
        return instance;
    }

    private ApiMapper(final RetrofitHelper helper) {
        this.mHelper = helper;
    }

    public void getImages(final OnImagesReceivedCallback callback) {
        mHelper.getService()
                .getImagesByQuery(API_KEY, ApiRequestParams.API_QUERY, ApiRequestParams.API_IMAGE_TYPE, ApiRequestParams.API_IMAGES_PER_PAGE)
                .enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.isSuccessful()) {
                            final List<ImageHit> imageHits = Objects.requireNonNull(response.body()).getImageHits();
                            callback.onImagesReceived(imageHits);
                        } else {
                            Log.e(TAG, response.message());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                        Log.e(TAG, t.getLocalizedMessage());
                    }
                });
    }
}
