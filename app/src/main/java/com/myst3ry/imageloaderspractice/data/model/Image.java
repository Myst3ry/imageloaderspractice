package com.myst3ry.imageloaderspractice.data.model;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.ImageType;

public final class Image implements ImageItem {

    private ImageType mImageType;

    private String mImageURL;

    public Image(final ImageType imageType, final String url) {
        this.mImageType = imageType;
        this.mImageURL = url;
    }

    @Override
    public ImageType getImageType() {
        return mImageType;
    }

    public String getImageURL() {
        return mImageURL;
    }
}
