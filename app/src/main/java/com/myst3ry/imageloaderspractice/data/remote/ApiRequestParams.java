package com.myst3ry.imageloaderspractice.data.remote;

public final class ApiRequestParams {

    public static final String API_QUERY = "workspace";
    public static final String API_IMAGE_TYPE = "photo";
    public static final int API_IMAGES_PER_PAGE = 100;

    private ApiRequestParams() { }
}
