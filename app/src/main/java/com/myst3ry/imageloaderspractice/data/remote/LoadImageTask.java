package com.myst3ry.imageloaderspractice.data.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public final class LoadImageTask extends AsyncTask<String, Void, Bitmap> {

    private static final int CODE_SUCCESS = 200;
    private final WeakReference<ImageView> mWeakImageViewReference;

    public LoadImageTask(final ImageView imageView) {
        mWeakImageViewReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        return loadImage(urls[0]);
    }

    @Override
    protected void onPostExecute(final Bitmap bitmap) {
        mWeakImageViewReference.get().setImageBitmap(bitmap);
    }

    private static Bitmap loadImage(final String urlString) {
        HttpURLConnection connection = null;
        try {
            final URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            final int responseCode = connection.getResponseCode();
            if (responseCode == CODE_SUCCESS) {
                return BitmapFactory.decodeStream(connection.getInputStream());
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
