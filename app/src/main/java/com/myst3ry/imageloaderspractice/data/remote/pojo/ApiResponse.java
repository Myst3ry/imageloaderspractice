package com.myst3ry.imageloaderspractice.data.remote.pojo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public final class ApiResponse {

	@SerializedName("hits")
	private List<ImageHit> imageHits;

	@SerializedName("total")
	private int total;

	@SerializedName("totalHits")
	private int totalHits;

	public List<ImageHit> getImageHits(){
		return imageHits;
	}

	public int getTotal(){
		return total;
	}

	public int getTotalHits(){
		return totalHits;
	}
}