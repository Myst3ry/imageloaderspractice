package com.myst3ry.imageloaderspractice.ui.holder;

import android.view.View;
import android.widget.ImageView;

import com.myst3ry.imageloaderspractice.R;

import butterknife.BindView;

public final class GlideImageHolder extends BaseImageHolder {

    @BindView(R.id.image_view)
    public ImageView mGlideImageView;

    public GlideImageHolder(View itemView) {
        super(itemView);
    }
}
