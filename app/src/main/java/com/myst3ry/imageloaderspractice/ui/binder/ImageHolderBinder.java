package com.myst3ry.imageloaderspractice.ui.binder;

import android.support.v7.widget.RecyclerView;

public abstract class ImageHolderBinder {

    protected final int mViewType;

    ImageHolderBinder(final int viewType) {
        this.mViewType = viewType;
    }

    public abstract void bindViewHolder(final RecyclerView.ViewHolder holder);

    public int getViewType() {
        return mViewType;
    }
}
