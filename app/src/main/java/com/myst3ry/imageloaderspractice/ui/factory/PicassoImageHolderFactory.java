package com.myst3ry.imageloaderspractice.ui.factory;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.ui.holder.PicassoImageHolder;

public final class PicassoImageHolderFactory implements ImageHolderFactory {

    @Override
    public PicassoImageHolder createImageHolder(@NonNull ViewGroup parent, @NonNull LayoutInflater inflater) {
        final View itemView = inflater.inflate(R.layout.item_image, parent, false);
        return new PicassoImageHolder(itemView);
    }
}
