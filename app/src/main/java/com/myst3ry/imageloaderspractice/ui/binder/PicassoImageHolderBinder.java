package com.myst3ry.imageloaderspractice.ui.binder;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.Toast;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.model.Image;
import com.myst3ry.imageloaderspractice.ui.holder.PicassoImageHolder;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public final class PicassoImageHolderBinder extends ImageHolderBinder {

    private final Image mImage;

    public PicassoImageHolderBinder(final ImageItem imageItem, final int viewType) {
        super(viewType);
        this.mImage = (Image) imageItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final PicassoImageHolder picassoHolder = (PicassoImageHolder) holder;

        Picasso.get()
                .load(mImage.getImageURL())
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.color.color_image_placeholder)
                .error(R.color.color_image_error)
                .into(picassoHolder.mPicassoImageView);

        picassoHolder.mPicassoImageView.setOnLongClickListener(v -> {
            Toast.makeText(v.getContext(), v.getContext().getString(R.string.tooltip_picasso), Toast.LENGTH_SHORT).show();
            return true;
        });
    }
}
