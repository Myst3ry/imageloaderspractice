package com.myst3ry.imageloaderspractice.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.remote.ApiMapper;
import com.myst3ry.imageloaderspractice.data.remote.RetrofitHelper;
import com.myst3ry.imageloaderspractice.data.remote.pojo.ImageHit;
import com.myst3ry.imageloaderspractice.ui.adapter.ImagesAdapter;
import com.myst3ry.imageloaderspractice.ImageTypesHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity {

    @BindView(R.id.images_rec_view)
    RecyclerView mImagesRecyclerView;

    private ImagesAdapter mImagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initAdapter();
        prepareRecyclerView();
        requestImagesFromApi();
    }

    private void initAdapter() {
        mImagesAdapter = new ImagesAdapter();
    }

    private void prepareRecyclerView() {
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.grid_span_count));
        mImagesRecyclerView.setLayoutManager(gridLayoutManager);
        mImagesRecyclerView.setAdapter(mImagesAdapter);
    }

    private void requestImagesFromApi() {
        final ApiMapper apiMapper = ApiMapper.getInstance(new RetrofitHelper());
        apiMapper.getImages(this::onImagesReceived);
    }

    private void onImagesReceived(final List<ImageHit> imageHits) {
        if (imageHits != null) {
            final List<ImageItem> images = ImageTypesHelper.mapToTypedImages(imageHits);
            setImagesToAdapter(images);
        }
    }

    private void setImagesToAdapter(final List<ImageItem> images) {
        if (mImagesAdapter != null) {
            mImagesAdapter.setImages(images);
            Toast.makeText(this, String.format(getString(R.string.images_loaded), images.size()), Toast.LENGTH_SHORT).show();
        }
    }
}
