package com.myst3ry.imageloaderspractice.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.ImageType;
import com.myst3ry.imageloaderspractice.ui.binder.FrescoImageHolderBinder;
import com.myst3ry.imageloaderspractice.ui.binder.GlideImageHolderBinder;
import com.myst3ry.imageloaderspractice.ui.binder.HttpImageHolderBinder;
import com.myst3ry.imageloaderspractice.ui.binder.ImageHolderBinder;
import com.myst3ry.imageloaderspractice.ui.binder.PicassoImageHolderBinder;
import com.myst3ry.imageloaderspractice.ui.factory.FrescoImageHolderFactory;
import com.myst3ry.imageloaderspractice.ui.factory.GlideImageHolderFactory;
import com.myst3ry.imageloaderspractice.ui.factory.HttpImageHolderFactory;
import com.myst3ry.imageloaderspractice.ui.factory.ImageHolderFactory;
import com.myst3ry.imageloaderspractice.ui.factory.PicassoImageHolderFactory;

import java.util.ArrayList;
import java.util.List;


public final class ImagesAdapter extends RecyclerView.Adapter {

    private final List<ImageHolderBinder> mImageBinders;
    private SparseArray<ImageHolderFactory> mImageFactories;

    public ImagesAdapter() {
        mImageBinders = new ArrayList<>();
        initFactories();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final ImageHolderFactory factory = mImageFactories.get(viewType);
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return factory.createImageHolder(parent, inflater);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ImageHolderBinder binder = mImageBinders.get(position);
        if (binder != null) {
            binder.bindViewHolder(holder);
        }
    }

    @Override
    public int getItemCount() {
        return mImageBinders.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mImageBinders.get(position).getViewType();
    }

    public void setImages(@NonNull final List<ImageItem> items) {
        mImageBinders.clear();
        for (final ImageItem item : items) {
            mImageBinders.add(generateBinder(item));
        }
        notifyDataSetChanged();
    }

    private void initFactories() {
        mImageFactories = new SparseArray<>();
        mImageFactories.put(ImageType.HTTP.getType(), new HttpImageHolderFactory());
        mImageFactories.put(ImageType.GLIDE.getType(), new GlideImageHolderFactory());
        mImageFactories.put(ImageType.PICASSO.getType(), new PicassoImageHolderFactory());
        mImageFactories.put(ImageType.FRESCO.getType(), new FrescoImageHolderFactory());
    }

    private ImageHolderBinder generateBinder(final ImageItem item) {
        switch (item.getImageType()) {
            case HTTP:
                return new HttpImageHolderBinder(item, ImageType.HTTP.getType());
            case PICASSO:
                return new PicassoImageHolderBinder(item, ImageType.PICASSO.getType());
            case GLIDE:
                return new GlideImageHolderBinder(item, ImageType.GLIDE.getType());
            case FRESCO:
                return new FrescoImageHolderBinder(item, ImageType.FRESCO.getType());
            default:
                return null;
        }
    }
}
