package com.myst3ry.imageloaderspractice.ui.binder;

import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.myst3ry.imageloaderspractice.GlideApp;
import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.model.Image;
import com.myst3ry.imageloaderspractice.ui.holder.GlideImageHolder;

public final class GlideImageHolderBinder extends ImageHolderBinder {

    private final Image mImage;

    public GlideImageHolderBinder(final ImageItem imageItem, final int viewType) {
        super(viewType);
        this.mImage = (Image) imageItem;
    }

    @Override
    public void bindViewHolder(final RecyclerView.ViewHolder holder) {
        final GlideImageHolder glideHolder = (GlideImageHolder) holder;

        GlideApp.with(holder.itemView.getContext())
                .load(mImage.getImageURL())
                .transition(DrawableTransitionOptions.withCrossFade())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.color.color_image_placeholder)
                .error(R.color.color_image_error)
                .into(glideHolder.mGlideImageView);

        glideHolder.mGlideImageView.setOnLongClickListener(v -> {
            Toast.makeText(v.getContext(), v.getContext().getString(R.string.tooltip_glide), Toast.LENGTH_SHORT).show();
            return true;
        });
    }
}
