package com.myst3ry.imageloaderspractice.ui.holder;

import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;
import com.myst3ry.imageloaderspractice.R;

import butterknife.BindView;

public final class FrescoImageHolder extends BaseImageHolder {

    @BindView(R.id.drawee_view)
    public SimpleDraweeView mFrescoDraweeView;

    public FrescoImageHolder(View itemView) {
        super(itemView);
    }
}
