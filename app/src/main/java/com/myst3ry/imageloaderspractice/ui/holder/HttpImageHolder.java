package com.myst3ry.imageloaderspractice.ui.holder;

import android.view.View;
import android.widget.ImageView;

import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.remote.LoadImageTask;

import butterknife.BindView;

public final class HttpImageHolder extends BaseImageHolder {

    @BindView(R.id.image_view)
    public ImageView mHttpImageView;

    public HttpImageHolder(View itemView) {
        super(itemView);
    }
}
