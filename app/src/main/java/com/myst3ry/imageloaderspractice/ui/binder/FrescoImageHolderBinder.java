package com.myst3ry.imageloaderspractice.ui.binder;

import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.model.Image;
import com.myst3ry.imageloaderspractice.ui.holder.FrescoImageHolder;

public final class FrescoImageHolderBinder extends ImageHolderBinder {

    private final Image mImage;

    public FrescoImageHolderBinder(final ImageItem imageItem, final int viewType) {
        super(viewType);
        this.mImage = (Image) imageItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final FrescoImageHolder frescoHolder = (FrescoImageHolder) holder;

        frescoHolder.mFrescoDraweeView.setImageURI(mImage.getImageURL());

        frescoHolder.mFrescoDraweeView.setOnLongClickListener(v -> {
            Toast.makeText(v.getContext(), v.getContext().getString(R.string.tooltip_fresco), Toast.LENGTH_SHORT).show();
            return true;
        });
    }
}
