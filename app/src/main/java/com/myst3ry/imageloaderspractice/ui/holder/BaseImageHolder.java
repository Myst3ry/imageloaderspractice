package com.myst3ry.imageloaderspractice.ui.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

abstract class BaseImageHolder extends RecyclerView.ViewHolder {

    BaseImageHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
