package com.myst3ry.imageloaderspractice.ui.factory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public interface ImageHolderFactory {

    RecyclerView.ViewHolder createImageHolder(@NonNull ViewGroup parent, @NonNull LayoutInflater inflater);
}
