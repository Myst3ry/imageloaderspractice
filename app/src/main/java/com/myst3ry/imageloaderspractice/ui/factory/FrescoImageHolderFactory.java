package com.myst3ry.imageloaderspractice.ui.factory;

import android.support.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.ui.holder.FrescoImageHolder;

public final class FrescoImageHolderFactory implements ImageHolderFactory {

    @Override
    public FrescoImageHolder createImageHolder(@NonNull ViewGroup parent, @NonNull LayoutInflater inflater) {
        final View itemView = inflater.inflate(R.layout.item_drawee, parent, false);
        return new FrescoImageHolder(itemView);
    }
}
