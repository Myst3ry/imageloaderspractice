package com.myst3ry.imageloaderspractice.ui.binder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.myst3ry.imageloaderspractice.ImageItem;
import com.myst3ry.imageloaderspractice.R;
import com.myst3ry.imageloaderspractice.data.model.Image;
import com.myst3ry.imageloaderspractice.data.remote.LoadImageTask;
import com.myst3ry.imageloaderspractice.ui.holder.HttpImageHolder;

public final class HttpImageHolderBinder extends ImageHolderBinder {

    private final Image mImage;

    public HttpImageHolderBinder(final ImageItem imageItem, final int viewType) {
        super(viewType);
        this.mImage = (Image) imageItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final HttpImageHolder httpHolder = (HttpImageHolder) holder;

        new LoadImageTask(httpHolder.mHttpImageView).execute(mImage.getImageURL());

        httpHolder.mHttpImageView.setOnLongClickListener(v -> {
            Toast.makeText(v.getContext(), v.getContext().getString(R.string.tooltip_http), Toast.LENGTH_SHORT).show();
            return true;
        });
    }
}
