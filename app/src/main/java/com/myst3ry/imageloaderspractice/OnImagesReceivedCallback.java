package com.myst3ry.imageloaderspractice;

import com.myst3ry.imageloaderspractice.data.remote.pojo.ImageHit;

import java.util.List;

public interface OnImagesReceivedCallback {

    void onImagesReceived(final List<ImageHit> imageHits);
}
