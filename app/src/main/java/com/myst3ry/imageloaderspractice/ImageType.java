package com.myst3ry.imageloaderspractice;

public enum ImageType {
    HTTP(0), PICASSO(1), GLIDE(2), FRESCO(3);

    private int mType;

    ImageType(final int type) {
        this.mType = type;
    }

    public int getType() {
        return mType;
    }
}
