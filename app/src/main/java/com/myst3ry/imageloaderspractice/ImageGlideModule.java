package com.myst3ry.imageloaderspractice;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class ImageGlideModule extends AppGlideModule {
}
