package com.myst3ry.imageloaderspractice;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public final class ImageLoadersApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initFresco();
    }

    private void initFresco() {
        Fresco.initialize(this);
    }
}
